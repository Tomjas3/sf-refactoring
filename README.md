### Refactoring'as

Karolis nebuvo labai geras programuotojas. Paliktas be priežiūros jis įgyvendino šią aplikaciją kaip mokėjo
ir paleido ją gyventi. Paviršutiniški testai praėjo, tačiau jos niekas daugiau nejudino ir į live nepaleido.
Vieną dieną, Karolis išėjo parūkyti ir niekada nebegrįžo į darbą. Taip jau būna dirbant Vandenilyje ¯\_(ツ)_/¯

Šiandien tu atėjai užimti Karolio vietos. Prieš tave - Karolio aplikacija. Niekas nedrįsom perimti šito
kodo, bet kaip naujokas tu laimėjai šią loteriją.

Tavo tikslas - pataisyti šį kodą taip, kad nebijotum toliau jo plėsti, būtu saugu paleisti į live aplinką
ir kolegų nemuštu šaltas prakaitas užmetus akį į tavo monitorių.

Sėkmės!

### Paleidimas
Bent vieną dalyką Karolis padarė gerai - paruošė `docker-compose.yml`, kad nereiktu sukti galvos
kaip paleisti aplikaciją.
```
docker-compose up
mysql -u root -proot -h 127.0.0.1 < fixture.sql
```

### Reikalavimai
* Originali serviso specifikacija jau pražuvo, tad negalima laužyti serviso public interface, t.y. response'ai bei url'ai negali kisti
* Visa kita gali kisti ir vietomis tikimasi rimtų pakeitimų su failų struktūra, naming ir t.t.
* Iš kodo reikia pašalinti sprendimus kuriuos jau padengia standartinis symfony stack'as
* Nėra apribojimų bibliotekoms/bundlesams, bet reikia išlaikyti gero kodo [wtf lygį](http://3.bp.blogspot.com/-ilMjE1Gh3Yg/VpUAmd-6TWI/AAAAAAAAAbg/-FJ08zxN42s/s1600/WFTPM.png)
* Užduotį atlikite pasidarę private fork'ą. Įvykdžius užduotį, reikia duoti read access'ą kdauzickas arba suarchyvuoti ir
persiųsti visą repozitoriją. Git istorijoje turėtu matytis žingsniai kuriais kodas buvo taisomas. Tai refactorinimo užduotis,
o ne išmetimo-ir-perrašymo.
