<?php

namespace App\Form\Type;

use App\Entity\Message;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('email', TextType::class)
			->add('types', CollectionType::class, [
				'entry_type' => TextType::class,
				'allow_add' => true,
			])
			->add('subject', TextType::class)
			->add('mail', TextType::class)
		;

		$builder->get('types')
			->addModelTransformer(new CallbackTransformer(

				function ($typesAsString) {
					if ($typesAsString === null) {
						return [];
					}
					// transform the array to a string
					return explode('|', $typesAsString);
				},
				function ($typesAsArray) {
					if (empty($typesAsArray)) {
						return '';
					}
					// transform the string back to an array
					return implode('|', $typesAsArray);
				}
			))
		;
	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Message::class,
			'csrf_protection' => false,
		]);
	}
}