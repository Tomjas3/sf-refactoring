<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\Type\MessageType;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends AbstractController
{
	private MessageRepository $messageRepository;
	private EntityManagerInterface $entityManager;

	public function __construct(MessageRepository $messageRepository, EntityManagerInterface $entityManager) {
		$this->messageRepository = $messageRepository;
		$this->entityManager = $entityManager;
	}

	/**
     * @Route("/message", name="message_list", methods={"HEAD", "GET"})
     */
    public function index() {

        return $this->json([
        	'success' => true,
	        'body' => $this->messageRepository->findAll(),
       ]);
    }

	/**
	 * @Route("/message/{id}", name="message_show", methods={"GET"})
	 * @param int $id
	 * @return JsonResponse|Response
	 */
    public function showMail(int $id) {

		$message = $this->messageRepository->findOneBy([
			'id' => $id
		]);

		if (!$message) {
			return new Response('Nothing found', Response::HTTP_NOT_FOUND);
		}
		return $this->json([
			'success' => true,
			'body' => $message->getMail(),
		]);
    }

	/**
	 * @Route("/message", name="message_create", methods={"POST"})
	 * @param Request $request
	 * @return JsonResponse|Response
	 */
    public function create(Request $request) {

	    $data = json_decode(
		    $request->getContent(), true
	    );

	    $form = $this->createForm(MessageType::class, new Message(), [
		    'csrf_protection' => false,
	    ]);
	    $form->submit($data);

	    if (!$form->isValid()) {
		    return new Response($form->getErrors());
	    }
	    $message = $form->getData();
	    $this->entityManager->persist($message);
	    $this->entityManager->flush();

	    return $this->json([
		    'success' => true,
		    'id'      => $message->getId(),
	    ]);
    }

	/**
	 * @Route("/message/{id}/status", name="message_get_status", methods={"GET"})
	 * @param int $id
	 * @return JsonResponse|Response
	 */
	public function showStatus(int $id) {

		$message = $this->messageRepository->findOneBy([
			'id' => $id
		]);

		if (!$message) {
			return new Response('Message not found', Response::HTTP_NOT_FOUND);
		}
		return $this->json([
			'success' => true,
			'status' => $message->getStatus(),
		]);
	}

	/**
	 * @Route("/message/{id}/cancel", name="message_cancel_status", methods={"POST"})
	 * @param int $id
	 * @return JsonResponse|Response
	 */
	public function cancelStatus(int $id) {

		$message = $this->messageRepository->findOneBy([
			'id' => $id
		]);
		if (!$message) {
			return new Response('Failed to retrieve mail', Response::HTTP_NOT_FOUND);
		}

		$message->setStatus('closed');
		$this->entityManager->persist($message);
		$this->entityManager->flush();

		return $this->json([
			'success' => true,
			'status' => $message->getStatus(),
		]);
	}

}
