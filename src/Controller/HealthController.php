<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HealthController extends AbstractController
{
	/**
	 * @Route("/healthz", name="health", methods={"GET"})
	 */
	public function status()
	{
		return new Response('', 200, ['pass' => true]);
	}
}
