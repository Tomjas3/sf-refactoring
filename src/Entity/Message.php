<?php

namespace App\Entity;

use App\Repository\MessageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="mail_queue")
 * @ORM\Entity(repositoryClass=MessageRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Message
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

	/**
	 * @ORM\Column(type="datetime")
	 */
    private $createdAt;

	/**
	 * @ORM\Column(type="text", options={"default"="new"})
	 * @Assert\NotBlank()
	 */
    private $status = 'new';

	/**
	 * @ORM\Column(type="text", length=65535)
	 * @Assert\Email()
	 */
    private $email;

	/**
	 * @ORM\Column(type="text", length=65535)
	 * @Assert\NotBlank()
	 */
	private $subject;

	/**
	 * @ORM\Column(type="text", length=65535)
	 * @Assert\NotBlank()
	 */
	private $mail;

	/**
	 * @ORM\Column(type="text", length=65535)
	 * @Assert\NotBlank()
	 */
	private $types;

	/**
	 * @ORM\Column(type="text", length=65535, nullable=true)
	 */
	private $receiver;

	public function getId(): ?int
    {
        return $this->id;
    }

	/**
	 * @return mixed
	 */
	public function getCreatedAt() {
		return $this->createdAt;
	}

	public function setCreatedAt(\DateTimeInterface $createdAt) {
		$this->createdAt = $createdAt;
	}

	/**
	 * @ORM\PrePersist
	 */
	public function setCreatedAtValue() {
		$this->createdAt = new \DateTime();
	}

	/**
	 * @return mixed
	 */
	public function getReceiver() {
		return $this->receiver;
	}

	/**
	 * @param mixed $receiver
	 */
	public function setReceiver($receiver): void {
		$this->receiver = $receiver;
	}

	/**
	 * @return string
	 */
	public function getStatus(): string {
		return $this->status;
	}

	/**
	 * @param string $status
	 */
	public function setStatus(string $status): void {
		$this->status = $status;
	}

	/**
	 * @return mixed
	 */
	public function getSubject() {
		return $this->subject;
	}

	/**
	 * @param mixed $subject
	 */
	public function setSubject($subject): void {
		$this->subject = $subject;
	}

	/**
	 * @return mixed
	 */
	public function getMail() {
		return $this->mail;
	}

	/**
	 * @param mixed $mail
	 */
	public function setMail($mail): void {
		$this->mail = $mail;
	}

	/**
	 * @return mixed
	 */
	public function getTypes() {
		return $this->types;
	}

	/**
	 * @param mixed $types
	 */
	public function setTypes($types): void {
		$this->types = $types;
	}

	/**
	 * @return mixed
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * @param mixed $email
	 */
	public function setEmail($email): void {
		$this->email = $email;
	}
}
