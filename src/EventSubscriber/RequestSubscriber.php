<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\KernelEvents;

class RequestSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::RESPONSE => ['response', 0]
        ];
    }

    public function response($event)
    {
        /** @var $event \Symfony\Component\HttpKernel\Event\FilterResponseEvent */
        $response = $event->getResponse();

        if ($response->headers->get('pass')) {
            return;
        }

//        if (@json_decode($response->getContent()) == false) {
//            $event->setResponse(new JsonResponse(['success' => false, 'message' => $response->getContent()]));
//        }
    }
}
