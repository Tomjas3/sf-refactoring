<?php

namespace App\Command;

use App\Entity\Message;
use App\Notification\MessageSentNotification;
use App\Repository\MessageRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Twig\Mime\NotificationEmail;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\Message\ChatMessage;
use Symfony\Component\Notifier\Notification\Notification;
use Symfony\Component\Notifier\NotifierInterface;
use Symfony\Component\Notifier\Recipient\Recipient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class SendMessagesCommand extends Command {
	protected static $defaultName = 'mail:send';

	private MessageRepository $messageRepository;

	private MailerInterface $mailer;
	/**
	 * @var HttpClientInterface
	 */
	private HttpClientInterface $httpClient;
	/**
	 * @var string
	 */
	private string $slackWebHookUrl;
	/**
	 * @var EntityManager
	 */
	private EntityManager $entityManager;


	public function __construct(MessageRepository $messageRepository, MailerInterface $mailer, HttpClientInterface $httpClient, EntityManager $entityManager, string $slackWebHookUrl) {
		$this->messageRepository = $messageRepository;
		$this->mailer = $mailer;
		$this->httpClient = $httpClient;
		$this->slackWebHookUrl = $slackWebHookUrl;
		$this->entityManager = $entityManager;
		parent::__construct();
	}

	protected function configure() {
		$this->setDescription('Sends new messages to email or slack');
	}

	protected function execute(InputInterface $input, OutputInterface $output): int {
		$io = new SymfonyStyle($input, $output);
		$messages = $this->messageRepository->findNotSentMessages();

		if (empty($messages)) {
			$io->writeln('No messages found that need to be sent');
			return 0;
		}

		foreach ($messages as $message) {
			/**
			 * @var Message $message
			 */
			$types = $message->getTypes();

			if (strpos($types, 'mail') !== false) {
				try {
					$this->mailer->send((new Email())
						->subject($message->getSubject())
						->text($message->getMail())
						->to($message->getEmail())
						->from('test@email.com')
					);
				} catch (\Symfony\Component\Mailer\Exception\TransportExceptionInterface $e) {
					$io->writeln('Exception found: '.$e->getMessage());
				}
			}

			if (strpos($types, 'slack') !== false) {
				try {
					$this->httpClient->request(
						'POST',
						$this->slackWebHookUrl, [
							'headers'     => [
								'Content-type' => 'application/json',
							],
							'verify_peer' => false,
							'json'        => [
								'icon_emoji' => ':exclamation:',
								'username'   => $message->getSubject(),
								'channel'    => 'general',
								'mrkdwn'     => true,
								'link_names' => true,
								'text'       => $message->getMail(),
							],
						]
					);
				} catch (TransportExceptionInterface $e) {
					$io->writeln('Exception found: '.$e->getMessage());
				}
			}
			$message = $this->messageRepository->find($message->getId());
//			$message->setStatus('sent');
			$this->entityManager->persist($message);
			$this->entityManager->flush();
		}
		$io->write('Ciki piki');

		return 0;
	}
}
